package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.ITaskRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task:list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {return list.get(index);}

    @Override
    public Task findOneByName(final String name) {
        for (final Task task:list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

}
