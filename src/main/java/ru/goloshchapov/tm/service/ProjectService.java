package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.IProjectRepository;
import ru.goloshchapov.tm.api.IProjectService;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Project;

import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.checkIndex;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project removeOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        final int size = findAll().size();
        if (index == null || index<0 || index >= size) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(String name) {
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneByIndex(Integer index) {
        final int size = findAll().size();
        if (index == null || index<0 || index >= size) return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(String name) {
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(name);
        return project;
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        final int size = findAll().size();
        if (index == null || index<0 || index>=size) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id) {
        if (isEmpty(id)) return null;
        return projectRepository.startProjectById(id);
    }

    @Override
    public Project startProjectByIndex(final Integer index) {
        final int size = projectRepository.size();
        if (checkIndex(index,size)) return projectRepository.startProjectByIndex(index);
        else return null;
    }

    @Override
    public Project startProjectByName(final String name) {
        if (isEmpty(name)) return null;
        return projectRepository.startProjectByName(name);
    }

    @Override
    public Project finishProjectById(final String id) {
        if (isEmpty(id)) return null;
        return projectRepository.finishProjectById(id);
    }

    @Override
    public Project finishProjectByIndex(final Integer index) {
        final int size = projectRepository.size();
        if (checkIndex(index,size)) return projectRepository.finishProjectByIndex(index);
        else return null;
    }

    @Override
    public Project finishProjectByName(final String name) {
        if (isEmpty(name)) return null;
        return projectRepository.finishProjectByName(name);
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (isEmpty(id)) return null;
        final Project project = findOneById(id);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String name, final Status status) {
        if (isEmpty(name)) return null;
        final Project project = findOneByName(name);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final int index, final Status status) {
        final int size = projectRepository.size();
        if (!checkIndex(index, size)) return null;
        final Project project = findOneByIndex(index);
        project.setStatus(status);
        return project;
    }

}
