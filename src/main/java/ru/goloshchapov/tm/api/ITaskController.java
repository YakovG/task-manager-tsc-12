package ru.goloshchapov.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskById();

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskById();

    void startTaskByName();

    void startTaskByIndex();

    void finishTaskById();

    void finishTaskByName();

    void finishTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByName();

    void changeTaskStatusByIndex();

}
